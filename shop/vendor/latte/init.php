<?php
require_once __DIR__ . '/vendor/autoload.php';

function renderTemplate($file, $dir, array $params = array())
{
	$latte = new Latte\Engine();
	$latte->setTempDirectory(__DIR__ . '/cache');
	$latte->render(__DIR__ . '/../../templates/' . $dir . "/" . $file . '.latte', $params);
}
